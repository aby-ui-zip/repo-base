## Interface: 90005
## Title: WeakAuras Archive
## Author: The WeakAuras Team
## Version: 3.4.1
## LoadOnDemand: 1
## SavedVariables: WeakAurasArchive
## Dependencies: WeakAuras

WeakAurasArchive.lua

## Title-zhCN: |cff880303[爱不易]|r WeakAuras 数据存档
## X-Vendor: AbyUI
