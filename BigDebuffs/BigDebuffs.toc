## Interface: 90005
## Title: BigDebuffs
## Notes: Increases the debuff size of crowd control effects on the Blizzard raid frames
## Version: v10.0
## Author: Jordon
## DefaultState: Enabled
## SavedVariables: BigDebuffsDB
## OptionalDeps: LibStub, CallbackHandler-1.0, Ace3, LibDualSpec-1.0
## X-Curse-Project-ID: 82697

Locales\Locales.xml
BigDebuffs.xml
Retail.lua
#BurningCrusade.lua
#Classic.lua
BigDebuffs.lua
Options.lua

## Title: |cff880303[爱不易]|r BigDebuffs 控制技能提示
## X-Vendor: AbyUI
